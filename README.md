# L10n for Markdown

## About

This is a simple tool to manage translations for a set of Markdown files, hosted at the [l10n-for-markdown][] repository.

[l10n-for-markdown]: https://gitlab.torproject.org/tpo/community/l10n-for-markdown

## Using

To use in your project, proceed as follows:

0. Install [po4a][] ([Debian package][]).
1. Put this repository as a Git submodule, or just copy it's files somewhere
   in your project.
2. Create a `l10n.sample` configuration in your project (use the provided one
   as an example, edit to suit your needs).
3. Run `make l10n-for-markdown-update-locales` or the
   `scripts/l10n-for-markdown-update-locales` directly. This will traverse
   all source Markdown files in your project, update the locales and also
   build translated Markdown files from your translated locales.
4. Edit your locales, and run the script again for the two-way sync between
   translations and Markdown files.

[po4a]: https://po4a.org
[Debian package]: https://tracker.debian.org/pkg/po4a
