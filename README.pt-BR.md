# L10n for Markdown

## Sobre

This is a simple tool to manage translations for a set of Markdown files,
hosted at the [l10n-for-markdown][] repository.

[l10n-for-markdown]:
https://gitlab.torproject.org/tpo/community/l10n-for-markdown

## Usando

Para usar no seu projecto, proceda da seguinte maneira:

0. Installe o [po4a] ([pacote Debian][]).
1. Coloque este repositório como um submódulo Git, ou apenas copie seus
   arquivos para algum lugar do seu projeto.
2. Crie um arquivo de configuração `l10n.sample` no seu projeto (use o
   arquivo fornecido como um exemplo e edite conforme suas necessidades).
3. Rode `make l10n-for-markdown-update locales` ou o script
   `scripts/l10n-for-markdown-update-locales` diretamente. Isto atravessará
   seu projeto em busca de todos os arquivos Markdown fonte, atualizará os
   locales e também construirá arquivos Markdown traduzidos a partir dos
   seus locales.
4. Edite seus locales, e rode o script novamente para que a sincronização de
   mão dupla entre traduções e arquivos Markdown.

[po4a]: https://po4a.org [Debian package]:
https://tracker.debian.org/pkg/po4a
